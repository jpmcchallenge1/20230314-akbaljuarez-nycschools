//
//  ContentView.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/14/23.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewModel = HighSchoolListViewModel()

    var body: some View {
        HighSchoolList()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
