//
//  HighSchool.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/14/23.
//

import Foundation
import MapKit



struct HighSchool: Identifiable, Codable {
    let id: String
    let schoolName: String
    let city: String
    let stateCode: String
    let zip: String
    let phoneNumber: String
    let website: String
    let overviewParagraph: String?
    let neighborhood: String?
    let totalStudents: String?
    let latitude: String?
    let longitude: String?
    let primary_address_line_1: String?
    
    /// Return a CLLocationCoordinate2D based on the latitude and longitude provided from the API response
    var coordinate: CLLocationCoordinate2D? {
        guard let latitude = latitude, let longitude = longitude else {
            return nil
        }
        return CLLocationCoordinate2D(latitude: Double(latitude) ?? 0, longitude: Double(longitude) ?? 0)
    }

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case city
        case stateCode = "state_code"
        case zip
        case phoneNumber = "phone_number"
        case website
        case overviewParagraph = "overview_paragraph"
        case neighborhood
        case totalStudents = "total_students"
        case latitude
        case longitude
        case primary_address_line_1
    }
}
