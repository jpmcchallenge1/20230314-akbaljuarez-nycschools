//
//  NetworkManager.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/14/23.
//

import Foundation


class API: APIClient {
    static let shared = API()
    
    init() {}
    
    private let baseUrl = "https://data.cityofnewyork.us/resource/"
    private let schoolsResource = "s3k6-pzi2.json"
    private let satScoresResource = "f9bf-2cp4.json"
    // Token has to be injected before runing the app
    private let xAppToken = "swIIqTq14b8roJ5SaftxbrFS8" //{Insert token or remove token from header}
    
    /// Fetch high schools
    /// - Parameter completion: will return the complition closure to the requester
    func fetchHighSchools(completion: @escaping (Result<[HighSchool], Error>) -> Void) {
        //Set a base url endpoint
        let endpoint = baseUrl + schoolsResource
        
        guard var url = URLComponents(string: endpoint) else {
            completion(.failure(APIError.invalidUrl))
            return
        }
        
        // Query parameters to only retrieve fields to be used
        url.queryItems = [
            URLQueryItem(name: "$select", value:"dbn,school_name,city,state_code,zip,phone_number,website,overview_paragraph,neighborhood,total_students,latitude,longitude,primary_address_line_1")
        ]
        var request = URLRequest(url: url.url!)
        print(request)
        
        // Add some headers to make the request, the token is not necesary but will allow more queries than not having it.
        request.setValue(xAppToken, forHTTPHeaderField: "X-App-Token")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type") // Set content type

        URLSession.shared.dataTask(with: request) { data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                completion(.failure(APIError.noData))
                return
            }
            
            do {
                //print(String(data: data, encoding: .utf8) ?? "")
                let decoder = JSONDecoder()
                //try to decode the response and send it back to the requester
                let highSchools = try decoder.decode([HighSchool].self, from: data)
                DispatchQueue.main.async {
                    completion(.success(highSchools))
                }
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
    
    /// Fetch the SAT Score by dbn and return it back to the requester
    /// - Parameters:
    ///   - dbn: this the id of each highschool on the database
    ///   - completion: will return the complition closure to the requester
    func fetchSATScores(for dbn: String, completion: @escaping (Result<SATScore, Error>) -> Void) {
        
        //Set a base url endpoint using the parameter provided (dbn)

        let endpoint = baseUrl + satScoresResource + "?dbn=\(dbn)"
        
        guard let url = URL(string: endpoint) else {
            completion(.failure(APIError.invalidUrl))
            return
        }
        
        var request = URLRequest(url: url)
        // Add some headers to make the request, the token is not necesary but will allow more queries than not having it.
        request.setValue(xAppToken, forHTTPHeaderField: "X-App-Token")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type") // Set content type

        URLSession.shared.dataTask(with: request) { data, _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                completion(.failure(APIError.noData))
                return
            }
            
            do {
                //print(String(data: data, encoding: .utf8) ?? "")
                let decoder = JSONDecoder()
                //try to decode the response and send it back to the requester
                let satScores = try decoder.decode([SATScore].self, from: data)
                if let firstScore = satScores.first {
                    DispatchQueue.main.async {
                        completion(.success(firstScore))
                    }
                } else {
                    completion(.failure(APIError.noSATScores))
                }
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
}

enum APIError: Error {
    case invalidUrl
    case noData
    case noSATScores
}
