//
//  APIClient.swift
//  20230314-AkbalJuarez-NYCSchoolsTests
//
//  Created by Juarez Martinez, Akbal on 3/16/23.
//

import Foundation

protocol APIClient {
    func fetchHighSchools(completion: @escaping (Result<[HighSchool], Error>) -> Void)
    func fetchSATScores(for dbn: String, completion: @escaping (Result<SATScore, Error>) -> Void)
}
