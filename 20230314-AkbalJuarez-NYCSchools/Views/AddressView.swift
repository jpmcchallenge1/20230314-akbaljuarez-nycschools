//
//  AddressView.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/15/23.
//

import SwiftUI
import MapKit

struct AddressView: View {
    @ObservedObject var viewModel: HighSchoolDetailViewModel
    let address:String
    
    private var region : Binding<MKCoordinateRegion> {
        Binding {
            viewModel.coordinateRegion
        } set: { region in
            DispatchQueue.main.async {
                viewModel.coordinateRegion = region
            }
        }
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("Address")
                .font(.headline)
            Text("\(address), \(viewModel.school.city), \(viewModel.school.stateCode)")
                .font(.caption)
                .fixedSize(horizontal: false, vertical: true)
            if let coordinate = viewModel.school.coordinate {
                let annotations = [
                    School(name: "School", coordinate: coordinate)
                ]
                Map(coordinateRegion: region, annotationItems: annotations){
                    MapMarker(coordinate: $0.coordinate)
                }
                .frame(height: 200)

            } else {
                Text("No location information available")
            }
        }
    }
}

struct School: Identifiable {
        let id = UUID()
        let name: String
        let coordinate: CLLocationCoordinate2D
}
