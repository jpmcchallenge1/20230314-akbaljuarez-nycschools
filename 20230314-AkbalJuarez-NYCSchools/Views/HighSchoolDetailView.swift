//
//  HighSchoolDetailView.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/14/23.
//

import SwiftUI

/// High School Detail View shows all the details of the school selected
struct HighSchoolDetailView: View {
    @ObservedObject var viewModel: HighSchoolDetailViewModel

    var body: some View {
        ScrollView{
            VStack(alignment: .leading, spacing: 20) {
                Text(viewModel.school.schoolName)
                    .font(.title)
                //If we have a SAT score we will show it if not we show a label saying we don't have it.
                if let satScores = viewModel.satScore {
                    ScoreDashboardView(mathAverage: satScores.mathAverage, readingAverage: satScores.criticalReadingAverage, writingAverage: satScores.writingAverage, numOfSatTestTakers: satScores.numOfSatTestTakers)
                } else {
                    Text("No SAT score information available")
                }
                Overview(paragraph: viewModel.school.overviewParagraph)
                // We make sure we have an addres and if we do we show the addres and we create a mapview to show the address as well
                if let address = viewModel.school.primary_address_line_1 {
                    AddressView(viewModel: viewModel, address: address)
                } else {
                    Text("No location information available")
                        .font(.subheadline)
                }
                
                Spacer()
            }
        }
        .padding()
        .navigationBarTitle(Text(viewModel.school.schoolName), displayMode: .inline)
        .onAppear {
            //We fetch the scores
            viewModel.fetchSATScores()
        }
    }
}

struct Overview: View {
    let paragraph:String?
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("Overview")
                .font(.headline)
            
            Text(paragraph ?? "")
                .font(.caption)
                .fixedSize(horizontal: false, vertical: true)
        }
    }
}



