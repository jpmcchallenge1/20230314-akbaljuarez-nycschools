//
//  APITest.swift
//  20230314-AkbalJuarez-NYCSchoolsTests
//
//  Created by Juarez Martinez, Akbal on 3/15/23.
//

import XCTest

class APITests: XCTestCase {

    var mockApiClient: MockAPIClient!

    override func setUpWithError() throws {
        mockApiClient = MockAPIClient(highSchools: [], satScore: SATScore(dbn: "01M292", mathAverage: "222", criticalReadingAverage: "228", writingAverage: "272", numOfSatTestTakers: "29"))
    }

    override func tearDownWithError() throws {
        mockApiClient = nil
    }

    func testFetchHighSchools() {
        // Given
        let expectation = self.expectation(description: "Completion handler invoked")
        let highSchools = [HighSchool(id: "2", schoolName: "22", city: "ss", stateCode: "ss", zip: "ss", phoneNumber: "ss", website: "ss", overviewParagraph: "ss", extracurricularActivities: "ss", admissionMethod: "ss", languageClasses: "ss", neighborhood: "ss", sharedSpace: "ss", ellPrograms: "ss", accessibility: "ss", startTime: "ss", endTime: "ss", gradeLow: "ss", gradeHigh: "ss", totalStudents: "ss", extracurricularSports: "ss", advancedPlacementCourses: "ss", latitude: "ss", longitude: "ss", primary_address_line_1: "ss")]
        mockApiClient.highSchools = highSchools

        // When
        mockApiClient.fetchHighSchools { result in
            switch result {
            case .success(let returnedHighSchools):
                XCTAssertEqual(returnedHighSchools.count, highSchools.count)
            case .failure(let error):
                XCTFail("Unexpected error: \(error)")
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 1.0, handler: nil)
        XCTAssertTrue(mockApiClient.fetchHighSchoolsCalled)


    }

    func testFetchSATScores() {
        // Given
        let expectation = self.expectation(description: "Completion handler invoked")
        let satScore = SATScore(dbn: "01M292", mathAverage: "222", criticalReadingAverage: "228", writingAverage: "272", numOfSatTestTakers: "29")
        mockApiClient.satScore = satScore

        // When
        mockApiClient.fetchSATScores(for: "01M292") { result in
            switch result {
            case .success(let returnedSATScore):
                XCTAssertEqual(returnedSATScore.mathAverage, satScore.mathAverage)
            case .failure(let error):
                XCTFail("Unexpected error: \(error)")
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 1.0, handler: nil)
        XCTAssertTrue(mockApiClient.fetchSATScoresCalled)


    }
}


