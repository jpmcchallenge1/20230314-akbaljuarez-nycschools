//
//  MockAPIClient.swift
//  20230314-AkbalJuarez-NYCSchoolsTests
//
//  Created by Juarez Martinez, Akbal on 3/16/23.
//

import Foundation

class MockAPIClient: APIClient {
    var highSchools: [HighSchool]
    var satScore: SATScore
    
    var fetchHighSchoolsCalled = false
    var fetchSATScoresCalled = false
    
    init(highSchools: [HighSchool], satScore: SATScore) {
        self.highSchools = highSchools
        self.satScore = satScore
    }
    
    func fetchSATScores(for dbn: String, completion: @escaping (Result<SATScore, Error>) -> Void) {
        completion(.success(satScore))
        fetchSATScoresCalled = true
 
    }
    
    func fetchHighSchools(completion: @escaping (Result<[HighSchool], Error>) -> Void) {
        completion(.success(highSchools))
        fetchHighSchoolsCalled = true
    }
}

enum MockAPIError: Error {
    case noData
}
